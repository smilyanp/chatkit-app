# Welcome to the Awesome Chatkit App!

This is an example single page application that aims to achieve the following:

 - To create a chat forum
 - To enable users of the chat forum to contribute to a chat session
 - The users can switch between chat rooms to have different conversations

Since this is a chat **forum** the application doesn't support one-to-one conversations, instead it provides **rooms** where multiple users can have different discussions.

# Technologies

The application is using:

 - **Chatkit APIs** for the back-end and authentication
 - **React.js** with the create-react app for the view layer
 - **MobX** for the state management

Since it is built with the free version of the [Chatkit (by Pusher)](https://docs.pusher.com/chatkit) APIs the application **doesn't provide real authentication with password protected users**. However, it does simulate authentication using the Chatkit API by simply allowing you to enter a user session with a username and the token retrieved.

Moreover, typically the create-react-app provides a very good initial setup for starting a project like this. However, often people have to "eject" from the setup to add additional functionality such as MobX. This is not the case with this app as it uses the useful technique [described by Leigh Halliday](https://www.youtube.com/watch?v=v3sJUzFXhz4).

# Demo
You can view the example [demo of the app here](https://chatkit-app.herokuapp.com/).

Pre-created users available to login with:

 - daniela
 - rob
 - john
 - andrea

You can simply use those to login and try the app. The rooms available to use are also pre-defined.


# Installation
In order to install and run the application locally, it assumes the following dependencies:

 - **Node.js**
 - **npm**
 - **git**
 - **webpack**

To install:

- `cd [project-folder]`
- `git clone https://smilyanp@bitbucket.org/smilyanp/chatkit-app.git ./`
- `npm install`
- `npm start` - to run in development mode

