import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

import './assets/Variables.css';
import './assets/index.css';
import './assets/libs/animate.min.css';

// Components
import App from './components/App';

// Store
import { Provider } from 'mobx-react';
import ChatStore from './stores/chatStore';

// Font-awesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { faPaperPlane, faBars } from '@fortawesome/free-solid-svg-icons';

library.add(faPaperPlane, faBars);

const Root = (
    <Provider ChatStore={ChatStore}>
        <App />
    </Provider>
);

ReactDOM.render(Root, document.getElementById('root'));
registerServiceWorker();
