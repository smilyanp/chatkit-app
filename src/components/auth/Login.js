import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import { observer } from 'mobx-react';
import LoginForm from './LoginForm';
import ChatStore from '../../stores/chatStore';
import Loader from '../chat/utils/Loader';

@observer

class Login extends Component {

    componentWillMount () {
        ChatStore.authenticate();
    }

    render () {
        const { from } = (
            this.props.location !== undefined && this.props.location.state !== undefined
        ) ? this.props.location.state 
        : { from: { pathname: `/app/room/${ChatStore.currentRoom}` } };
        return (
            !ChatStore.isAuthenticated ? (
                <div>
                    <LoginForm />
                    <Loader />
                </div>
            ) : (
                <Redirect to={from} />
            )
        )
    }
}

export default Login;