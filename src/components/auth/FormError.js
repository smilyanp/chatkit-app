import React from 'react';

const FormError = props => {
    return (
        <div className="error">
            {props.errorMessage}
        </div>
    )
}

export default FormError;