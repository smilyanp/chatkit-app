import React, { Component } from 'react';
import { observer } from 'mobx-react';
import '../../assets/auth/Login.css';
import ChatStore from '../../stores/chatStore';
import FormError from './FormError';

@observer

class LoginForm extends Component {

    constructor() {
        super();
        this.state = {
            username: '',
            animatedError: false
        }
    }

    handleChange(e) {
        this.setState({
            username: e.target.value
        });
    }

    async handleSubmit (e) {
        e.preventDefault();

        if (this.state.username.trim().length === 0) {
            this.setState({
                username: '',
                animatedError: true
            });

            setTimeout(() => {
                this.setState({ animatedError: false });
            }, 1000);

            return;
        }

        await ChatStore.login(this.state.username);

        this.setState({
            username: ''
        });
    }

    render() {
        const animated = this.state.animatedError ? 'shake animated' : '';
        const errors = ChatStore.formErrors.map(error => error.message + ' ');
        return (
            <div className="login bounceInDown animated">
                <h1>Login</h1>
                <form method="post" onSubmit={this.handleSubmit.bind(this)} className={animated}>
                    <div className="form-row">
                        <input 
                            type="text" 
                            value={this.state.username}
                            onChange={this.handleChange.bind(this)}
                            placeholder="Username" />
                        
                        {errors.map((error, index) => (
                            <FormError errorMessage={error} key={index} />
                        ))}
                    </div>
                    <div className="form-row">
                        <button>
                            Let me in
                        </button>
                    </div>
                </form>
            </div>
        )
    }
}

export default LoginForm;