import React from 'react';
import { withRouter } from 'react-router-dom';
import { observer } from 'mobx-react';
import '../../assets/chat/Logout.css';
import ChatStore from '../../stores/chatStore';

const Logout = withRouter(observer(({ history, ...rest }) => (
    <div className="session-info">
        <span className="loggedin-user">
            Logged in as {ChatStore.currentUser.id} 
        </span>
        <button 
            className="logout-btn"
            onClick={() => ChatStore.logout(history)}>
            Logout
        </button>
    </div>
)));

export default Logout;