import React, { Component } from 'react';
import { observer } from 'mobx-react';
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";

import Chat from './chat/Chat';
import Login from './auth/Login';

@observer

class App extends Component {
    render() {
        return (
            <Router>
                <div className="app">
                    <Route path="/(login|)/" component={Login} />
                    <Route path={`/app/room/:roomId`} component={Chat} />
                </div>
            </Router>
        );
    }
}

export default App;
