const tokenUrl = 'https://us1.pusherplatform.io/services/chatkit_token_provider/v1/2708a024-df75-42ae-9895-3fdb276eb5c5/token';
const instanceLocator = 'v1:us1:2708a024-df75-42ae-9895-3fdb276eb5c5';

export { tokenUrl, instanceLocator };