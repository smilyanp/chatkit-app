import React from 'react';
import { Link } from 'react-router-dom';
import { observer } from 'mobx-react';
import ChatStore from '../../../stores/chatStore';

const Room = (observer((props) => {
    let active = props.room.id === ChatStore.currentRoom ? 'active' : '';
    return (
        <li className={"room " + active}>
            <Link to={`/app/room/${props.room.id}`}>
                #{props.room.name}
            </Link>
        </li>
    )
}));

export default Room;