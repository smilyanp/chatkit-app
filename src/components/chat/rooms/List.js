import React from 'react';
import Room from './Room';
import { observer } from 'mobx-react';
import ChatStore from '../../../stores/chatStore';
import '../../../assets/chat/Rooms.css';

const RoomList = (observer(() => {
    let sortedRooms = [...ChatStore.availableRooms, ...ChatStore.joinedRooms].sort((a, b) => a.id - b.id);
    return (
        <div className="rooms">
            <h3 className="title">Rooms</h3>
            <ul>
                {sortedRooms.map(room => (
                    <Room room={room} key={room.id} />
                ))}
            </ul>
        </div>
    )
}));

export default RoomList;