import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { observer } from 'mobx-react';
import Message from './Message';
import ChatStore from '../../../stores/chatStore';
import '../../../assets/chat/Messages.css';
import Loader from '../utils/Loader';

@observer

class MessagesList extends Component {

    componentDidMount () {
        this.scrollToBottom();
    }
    
    componentDidUpdate () {
        this.scrollToBottom();
    }
    
    // Scroll to the bottom of the messages list
    scrollToBottom () {
        const currentComponent = ReactDOM.findDOMNode(this);
        currentComponent.scrollTop = currentComponent.scrollHeight;
    }
    
    render() {
        return (
            <div className="messages">
                <div>
                    <ul>
                        {ChatStore.messages.map((message, index) => (
                            <Message 
                                message={message} 
                                currentUser={ChatStore.currentUser.id} 
                                key={index} />
                        ))}
                    </ul>
                </div>
                <Loader />
            </div>
        )
    }
}

export default MessagesList;