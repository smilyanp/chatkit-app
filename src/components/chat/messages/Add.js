import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ChatStore from '../../../stores/chatStore';
import '../../../assets/chat/AddMessage.css';

@observer

class AddMessage extends Component {

    constructor () {
        super();
        this.state = {
            message: '',
            animatedError: false
        }
    }

    handleChange(e) {
        this.setState({
            message: e.target.value
        })
    }

    handleSubmit(e) {
        e.preventDefault();

        if (this.state.message.trim().length === 0) {
            this.setState({ 
                animatedError: true
            });

            setTimeout(() => {
                this.setState({ animatedError: false });
            }, 1000);

            return;
        }

        ChatStore.sendMessage(this.state.message);
        this.setState({ message: ''});
    }

    render() {
        const animation = this.state.animatedError ? 'shake animated' : '';
        return (
            <div className="add-message-wrapper">
                <form onSubmit={this.handleSubmit.bind(this)} className="add-message-form">
                    <input 
                        className={"add-message-input " + animation}
                        type="text" 
                        autoFocus
                        placeholder="Write a message..." 
                        value={this.state.message} 
                        onChange={this.handleChange.bind(this)} />

                    <button className="add-message-btn">
                        <FontAwesomeIcon icon="paper-plane" />
                    </button>
                </form>
            </div>
        )
    }
}

export default AddMessage;