import React from 'react';
import Moment from 'react-moment';

const Message = ({message, currentUser, ...props}) => {
    const type = message.senderId === currentUser ? 'sent' : 'reply';
    return (
        <li className={type + ' message-wrapper fadeIn animated'}>
            <div className="message-author-avatar">
                <img src="https://34yigttpdc638c2g11fbif92-wpengine.netdna-ssl.com/wp-content/uploads/2016/09/default-user-img-768x768.jpg" alt="" />
            </div>
            <div className="message-content">
                <div className="message-info">
                    <div className="message-author">
                        {message.senderId}
                    </div>
                    <div className="message-time">
                        <Moment format="DD-MM-YYYY HH:mm">
                            {message.createdAt}
                        </Moment>
                    </div>
                </div>
                <div className="message-text">
                    {message.text}
                </div>
            </div>
        </li>
    )
}

export default Message;