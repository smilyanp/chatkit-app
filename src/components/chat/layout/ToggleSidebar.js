import React from 'react';
import { observer } from 'mobx-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ChatStore from '../../../stores/chatStore';
import '../../../assets/chat/ToggleSidebar.css';

const ToggleSidebar = (observer(() => {
    return (
        <button 
            className="toggle-sidebar-btn visible-mobile" 
            onClick={() => ChatStore.toggleSidebar()}>
                <FontAwesomeIcon icon="bars" />
        </button>
    )
}));

export default ToggleSidebar;