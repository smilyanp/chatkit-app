import React from 'react';
import Logout from '../../auth/Logout';
import ToggleSidebar from './ToggleSidebar';

const Header = () => (
    <div className="header">
        <ToggleSidebar />
        <Logout />
    </div>
);

export default Header;