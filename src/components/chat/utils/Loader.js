import React from 'react';
import { observer } from 'mobx-react';
import '../../../assets/utils/Loader.css';
import ChatStore from '../../../stores/chatStore';

const Loader = observer(() => {
    let toggleLoader = ChatStore.isLoading  ? 'show-loader' : 'hide-loader';
    return (
        <div className={ 'loader-wrapper ' + toggleLoader }>
            <div className="lds-ripple"><div></div><div></div></div>
        </div>
    );
});

export default Loader;