import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import { observer } from 'mobx-react';
import ChatStore from '../../stores/chatStore';

import '../../assets/chat/Layout.css';

import Header from './layout/Header';
import MessagesList from './messages/List';
import AddMessage from './messages/Add';
import RoomsList from './rooms/List';

@observer

class Chat extends Component {

    componentWillMount () {
      let roomId = Number(this.props.match.params.roomId);
      if (ChatStore.currentRoom === null || ChatStore.currentRoom !== roomId) {
        ChatStore.currentRoom = roomId;
      }
    }

    componentDidUpdate (prevProps, prevState, snapshot) {
      let roomId = Number(this.props.match.params.roomId);
      if (Number(prevProps.match.params.roomId) !== roomId) {
        ChatStore.subscribeToRoom(roomId);
      }
    }

    render() {
      if (!ChatStore.isAuthenticated) {
        return <Redirect to={{ pathname: "/login", state: { from: this.props.location } }}/>
      }

      const toggleSidebarClass = ChatStore.showSidebar ? 'sidebar-visible' : 'sidebar-hidden';

      return (
          <div className={"full-window-container " + toggleSidebarClass}>
            <div className="sidebar">
              <RoomsList />
            </div>
            <Header />
            <div className="messages-wrapper">
              <MessagesList key={toggleSidebarClass} />
            </div>
            <AddMessage />
          </div>
      );
    }
}

export default Chat;
