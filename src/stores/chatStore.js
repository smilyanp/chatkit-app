import Chatkit from '@pusher/chatkit';
import { tokenUrl, instanceLocator } from '../components/chat/config';
import { action, observable, computed } from 'mobx';

class ChatStore {

    @observable messages = [];
    @observable availableRooms = [];
    @observable joinedRooms = [];
    @observable currentRoom = null;
    @observable ChatManager = {};
    @observable currentUser = {};
    @observable isAuthenticated = false;
    @observable isLoading = true;
    @observable showSidebar = true;
    @observable formErrors = [];


    /**
     * AUTHENTICATE
     * 
     * Check if the user is already logged in
     * If not set the isAuthenticated to false
     * which will redirect them to the login page
     * ==============================================
     */

    @action authenticate = async () => {

        const userToken = this.getUserToken;

        if (userToken.token === null || !userToken.token) {

            this.isAuthenticated = false;
            this.isLoading = false;

        } else {

            await this.login(userToken.userId);

        }
    }
    

    /**
     * getUserToken
     * 
     * Returns an object of the user token details
     * stored in localStorage
     */

    @computed get getUserToken() {

        return {
            'token': localStorage.getItem("userToken"),
            'expires': localStorage.getItem("userTokenExpiration"),
            'userId': localStorage.getItem("userId")
        }

    }
    

    /**
     * MESSAGES
     * ==============================================
     */
    

    /**
     * onNewMessage
     * 
     * Updates the state with new messages by merging
     * the new message with the existing ones
     */
    
    @action onNewMessage = message => {
        
        this.messages = [...this.messages, message];

    }


    /**
     * sendMessage
     * 
     * Using the method available with the Chatkit's currentUser
     * it sends a give message to the local state's currentRoom
     */
    
    @action sendMessage = message => {
        
        this.currentUser.sendMessage({
            text: message,
            roomId: this.currentRoom
        });

    }


    /**
     * LOGOUT
     * ========================================
     */


    /**
     * logout
     * 
     * Using Chatkit's currentUSer method it 
     * disconnects the user from Chatkit.
     * Then it cleans up the localStorage containing the user token.
     * And finally it redirects to the login page.
     */
    
    @action logout = async (history, event) => {
        
        this.isLoading = true;

        try {

            this.currentUser.disconnect();

            localStorage.removeItem('userToken');
            localStorage.removeItem('userTokenExpiration');
            localStorage.removeItem('userId');

            history.push('/login');

        } catch (error) {

            console.log('Error on trying to logout ', error);
            this.isLoading = false;

        }

    }


    /**
     * LOGIN - Start the authentication process
     * ========================================
     */


    /**
     * login
     * 
     * Called with a username (form the <Login /> component)
     * and attempts to creates a Chatkit ChatManger for that username
     */
    
    @action login = async username => {
        this.isLoading = true;
        await this.createChatManager(username);
    }


    /**
     * createChatManager
     * 
     * Attempts to create a Chatkit Chatmanager with
     * the demo instanceLocator and token provider.
     * If successful it stores the returned token in local storage,
     * and sets the ChatManager on the local state.
     * Then it tries to connect to the Chatkit ChatManager
     */
    
    @action createChatManager = async userId => {

        const ChatManager = new Chatkit.ChatManager({
            instanceLocator,
            userId: userId,
            tokenProvider: new Chatkit.TokenProvider({
                url: tokenUrl
            })
        });

        const tokenInstance = ChatManager.presenceInstance.tokenProvider;
        this.cacheUserToken(
            await tokenInstance.fetchToken(),
            tokenInstance.cacheExpiresAt,
            tokenInstance.userId,
        );

        this.ChatManager = ChatManager;
        await this.connectToChat();

    }


    /**
     * cacheUserToken
     * 
     * Sets the give user token details in the browser's localStorage
     */
    
    @action cacheUserToken = (token, tokenExpiration, userId) => {

        localStorage.setItem("userToken", token);
        localStorage.setItem("userTokenExpiration", tokenExpiration);
        localStorage.setItem("userId", userId);

    }


    /**
     * connectToChat
     * 
     * Attempts to connect to the Chatkit ChatMnager,
     * which if successful creates a session for the currentUser.
     * This currentUser object provides all the API methods to interact
     * with Chatkit.
     *
     * If successful it gets the user's available and joined rooms,
     * and sets them in the local state.
     *
     * Then it connects to the first available room. 
     */
    
    @action connectToChat = async () => {
        
        try {

            const currentUser = await this.ChatManager.connect();
            this.currentUser = currentUser;
            await this.getAvailableRooms();
            
            if (this.currentRoom === null || Number.isNaN(this.currentRoom)) {
                this.currentRoom = this.joinedRooms[0].id;
            }
            
            await this.subscribeToRoom(this.currentRoom);
            this.formErrors = [];
            this.isAuthenticated = true;
            this.isLoading = false;

        } catch (error) {

            console.log('Error on connection to the ChatManager ', error);
            this.formErrors = [];
            this.formErrors.push({
                status: error.statusCode,
                message: error.info.error_description
            });
            this.isLoading = false;

        }

    }


    /**
     * getAvailableRooms
     * 
     * Attempts to get the user's available and joined rooms
     * using the currentUser's Chatkit methods.
     * 
     * If successful it sets them in the local state.
     */
    
    @action getAvailableRooms = async () => {

        try {

            const availableRooms = await this.currentUser.getJoinableRooms();
            this.availableRooms = availableRooms;
            this.joinedRooms = this.currentUser.rooms;

        } catch (error) {

            console.log('Error on getting available rooms ', error)

        }

    }


    /**
     * subscribeToRoom
     * 
     * Attempts to subscribe the user to a room 
     * using Chatkit's methods available with the currentUser.
     *
     * If successful it binds a listener to the provided hooks
     * so that any new message from can be received and added to the local state.
     */
    
    @action subscribeToRoom = async roomId => {
        
        if (this.currentUser !== undefined) {
            this.isLoading = true;
            
            try {

                this.messages = [];
                const room = await this.currentUser.subscribeToRoom({
                    roomId,
                    messageLimit: 50,
                    hooks: {
                        onNewMessage: message => this.onNewMessage(message)
                    }
                });

                this.currentRoom = room.id;
                await this.getAvailableRooms();
                this.isLoading = false;

            } catch (error) {

                console.log('Error on subscribing to a room ', error);
                this.isLoading = false;

            }
        }

    }


    /**
     * setCurrentRoom
     * 
     * Sets the local state's current room ID to a give integer
     * This is called from the <Chat /> component when the props
     * with the new room ID update from the router.
     */
    
    @action setCurrentRoom = roomId => {

        this.currentRoom = roomId;

    }


    /**
     * LAYOUT
     * ========================================
     */


    /**
     * toggleSidebar
     * 
     * Sets the sidebar state so that the UI can show / hide it.
     * This is only used in the responsive mobile version of the app.
     */

    @action toggleSidebar = () => {

        this.showSidebar = this.showSidebar ? false : true;

    }
}

export default new ChatStore();